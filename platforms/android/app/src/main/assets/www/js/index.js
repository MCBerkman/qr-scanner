/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');

    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

function showOrHide(elementId) {
         var x = document.getElementById(elementId);
         if (x.style.display === "none") {
             x.style.display = "block";
         } else {
             x.style.display = "none";
         }
}

function showQR() {
         window.QRScanner.prepare(onDone);
         showOrHide("buttons1")
         showOrHide("buttons2")
         window.QRScanner.show(function(status){
                          console.log(status);
         });
         //alert("Shown")
}
function scanQR() {
          alert("Scanning")
          window.QRScanner.scan(displayContents);
}
function onDone(err, status){
          if (err) {
                 console.error(err);
          }
          if (status.authorized) {
                //alert("Authorized")
          } else{
                 alert("App doesn't have permission to use Camera.")
          }
}

function displayContents(err, text){
               alert("Displaying...")
               const regex = /\s*h|Ht|Tt|Tp/;
               if( regex.test(text) ){
                  //if its a URL, will automatically go to page.
                  window.open(text, '_system', 'location=yes');
               }
               else{// otherwise will display contents
                   if(err){
                         // an error occurred, or the scan was canceled (error code `6`)
                         alert("Error:" + JSON.stringify(err))
                   } else {
                         // maybe to prevent it turn it into json like what dad did in myApp
                         alert(text)
                   }
               }
}
app.initialize();